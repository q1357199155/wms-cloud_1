/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.7.34 : Database - ry-wms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ry-wms` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `ry-wms`;

/*Table structure for table `wms_account` */

DROP TABLE IF EXISTS `wms_account`;

CREATE TABLE `wms_account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) NOT NULL COMMENT '账户编号',
  `account_name` varchar(30) NOT NULL COMMENT '账户名称',
  `current_balance` decimal(10,6) DEFAULT NULL COMMENT '当前余额',
  `first_get` decimal(10,6) DEFAULT NULL COMMENT '期初余额',
  `account_type` varchar(30) NOT NULL COMMENT '账户类型',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='账户信息表';

/*Data for the table `wms_account` */

/*Table structure for table `wms_address` */

DROP TABLE IF EXISTS `wms_address`;

CREATE TABLE `wms_address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(64) NOT NULL COMMENT '地址简称',
  `phone` varchar(30) NOT NULL COMMENT '联系电话',
  `contacts` varchar(30) NOT NULL COMMENT '联系人',
  `postal_code` varchar(30) DEFAULT NULL COMMENT '邮政编码',
  `is_default` bit(1) DEFAULT NULL COMMENT '是否默认',
  `province` varchar(30) NOT NULL COMMENT '省',
  `city` varchar(30) NOT NULL COMMENT '市',
  `area` varchar(30) NOT NULL COMMENT '区',
  `address_detail` varchar(500) NOT NULL COMMENT '详细地址',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='发货地址信息表';

/*Data for the table `wms_address` */

/*Table structure for table `wms_check_inventory` */

DROP TABLE IF EXISTS `wms_check_inventory`;

CREATE TABLE `wms_check_inventory` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sn` varchar(64) DEFAULT NULL COMMENT '盘点单号',
  `status` varchar(8) DEFAULT NULL COMMENT '状态:0-待提交,1-待审批,2-已批准',
  `worker_id` int(11) DEFAULT NULL COMMENT '盘点人id',
  `warehouse_id` bigint(20) DEFAULT NULL COMMENT '仓库id',
  `file_path` varchar(500) DEFAULT NULL COMMENT '上传盘点文件路径',
  `approval_time` datetime DEFAULT NULL COMMENT '审批时间',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='盘点表';

/*Data for the table `wms_check_inventory` */

/*Table structure for table `wms_check_inventory_item` */

DROP TABLE IF EXISTS `wms_check_inventory_item`;

CREATE TABLE `wms_check_inventory_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `check_inventory_id` int(11) DEFAULT NULL COMMENT '盘点单id',
  `sku_id` int(11) DEFAULT NULL COMMENT '商品sku',
  `inventory_type` varchar(8) DEFAULT NULL COMMENT '库存变动类型(4-盘盈入库,5-盘亏出库)',
  `warehouse_id` int(11) DEFAULT NULL COMMENT '仓库id',
  `qty` decimal(20,6) DEFAULT NULL COMMENT '盘点库存数量',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='盘点明细表';

/*Data for the table `wms_check_inventory_item` */

/*Table structure for table `wms_customer` */

DROP TABLE IF EXISTS `wms_customer`;

CREATE TABLE `wms_customer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) NOT NULL COMMENT '客户编号',
  `customer_name` varchar(30) NOT NULL COMMENT '客户名称',
  `customer_type` varchar(8) DEFAULT NULL COMMENT '客户类别',
  `customer_level` varchar(8) DEFAULT NULL COMMENT '客户等级',
  `balance_date` datetime DEFAULT NULL COMMENT '余额日期',
  `first_get` decimal(20,6) DEFAULT NULL COMMENT '期初应收',
  `first_pre_get` decimal(20,6) DEFAULT NULL COMMENT '期初预收',
  `tax_identity` varchar(64) DEFAULT NULL COMMENT '纳税人识别号',
  `bank_info` varchar(256) DEFAULT NULL COMMENT '开户银行',
  `bank_num` varchar(256) DEFAULT NULL COMMENT '银行账号',
  `seller_id` int(11) DEFAULT NULL COMMENT '销售人员id',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='客户信息表';

/*Data for the table `wms_customer` */

insert  into `wms_customer`(`id`,`sn`,`customer_name`,`customer_type`,`customer_level`,`balance_date`,`first_get`,`first_pre_get`,`tax_identity`,`bank_info`,`bank_num`,`seller_id`,`del_flag`,`tenant_id`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (2,'CT1620492327167','广东牛牛公司','2','2','2021-01-01 00:00:00','4545.440000','4545.440000','34234','中国银行','2323323',2,'\0',10,'','2021-05-09 00:45:27','admin','2021-05-11 05:52:36',NULL),(3,'CT1620492327168','广东牛牛公司','2','2','2021-01-01 00:00:00','4545.440000','4545.440000','34234','中国银行','2323323',1,'',10,'','2021-05-09 00:45:27','','2021-05-09 00:55:02',NULL),(4,'CT1620492327169','广东牛牛公司','2','2','2021-01-01 00:00:00','4545.440000','4545.440000','34234','中国银行','2323323',1,'',10,'','2021-05-09 00:45:27','','2021-05-09 00:55:02',NULL),(5,'CT1620651791627','rrrr','1','1','2021-05-10 00:00:00','3233.000000','2233.000000','rrere','erre','rererr',2,'\0',NULL,'admin','2021-05-10 21:03:12','admin','2021-05-11 04:27:30',NULL),(6,'CT1620683290204','xfd','1','1','2021-05-11 00:00:00','33.000000','23.000000','ewew','ewe','wewe',7,'\0',NULL,'admin','2021-05-11 05:48:10','admin','2021-05-11 05:48:24',NULL),(7,'CT1621585411101','侵权','1','1',NULL,'1.000000','1.000000','12122','飒飒','1212',1,'\0',NULL,'admin','2021-05-21 16:23:31','',NULL,NULL),(8,'CT1628543935721','sssssss','1','1','2021-08-10 00:00:00','323.000000','2323.000000','233','3r2r','2r2r',12,'\0',NULL,'admin','2021-08-10 05:18:56','',NULL,NULL);

/*Table structure for table `wms_customer_contacts` */

DROP TABLE IF EXISTS `wms_customer_contacts`;

CREATE TABLE `wms_customer_contacts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(64) NOT NULL COMMENT '联系人姓名',
  `phone` varchar(30) NOT NULL COMMENT '联系人手机',
  `tel` varchar(64) DEFAULT NULL COMMENT '坐机',
  `email` varchar(256) DEFAULT NULL COMMENT '邮箱/QQ/微信',
  `address` varchar(256) DEFAULT NULL COMMENT '地址',
  `is_default` bit(1) DEFAULT NULL COMMENT '是否默认',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `customer_id` int(11) DEFAULT NULL COMMENT '客户ID',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='客户联系人信息表';

/*Data for the table `wms_customer_contacts` */

insert  into `wms_customer_contacts`(`id`,`name`,`phone`,`tel`,`email`,`address`,`is_default`,`del_flag`,`customer_id`,`tenant_id`,`create_by`,`create_time`,`update_by`,`update_time`) values (5,'关胜6','44444','4444','555','ewrwe','','\0',2,10,'','2021-05-09 00:45:27','','2021-05-11 05:52:36'),(6,'关胜','12222','2323','223','ewrwe','','\0',2,10,'','2021-05-09 00:45:27','','2021-05-11 05:52:36'),(7,'牛大力','12222','2323','223','ewrwe','\0','\0',2,10,'','2021-05-09 00:55:02','','2021-05-11 05:52:36'),(8,'erer','4333','','','','\0','\0',5,NULL,'admin','2021-05-10 21:03:12','','2021-05-11 04:27:30'),(9,'rerer','43434','','','','\0','\0',5,NULL,'admin','2021-05-10 21:03:12','','2021-05-11 04:27:30'),(10,'dfsfs','233','233','23','32','','\0',6,NULL,'admin','2021-05-11 05:48:10','','2021-05-11 05:48:24'),(11,'23r','r23','23r','2','3r','\0','\0',6,NULL,'admin','2021-05-11 05:48:10','','2021-05-11 05:48:24'),(12,'2r2r','23r2','2rr','2r','2r2r','\0','\0',8,NULL,'admin','2021-08-10 05:18:56','',NULL);

/*Table structure for table `wms_inventory` */

DROP TABLE IF EXISTS `wms_inventory`;

CREATE TABLE `wms_inventory` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sku_id` int(11) DEFAULT NULL COMMENT 'skuId',
  `warehouse_id` int(11) DEFAULT NULL COMMENT '仓库id',
  `qty` decimal(20,6) DEFAULT NULL COMMENT '库存数量',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `version` int(11) DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='库存信息表';

/*Data for the table `wms_inventory` */

insert  into `wms_inventory`(`id`,`sku_id`,`warehouse_id`,`qty`,`del_flag`,`tenant_id`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`,`version`) values (7,193,2,'31.000000','\0',6,'13326778347','2021-08-06 05:10:13','admin','2021-08-09 10:34:47',NULL,NULL),(8,194,2,'68.000000','\0',6,'13326778347','2021-08-06 05:10:13','admin','2021-08-09 10:34:47',NULL,NULL),(9,201,2,'55.000000','\0',6,'13326778347','2021-08-06 05:10:13','',NULL,NULL,NULL),(10,195,2,'30.000000','\0',NULL,'admin','2021-08-09 10:11:42','',NULL,NULL,NULL);

/*Table structure for table `wms_inventory_log` */

DROP TABLE IF EXISTS `wms_inventory_log`;

CREATE TABLE `wms_inventory_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) NOT NULL COMMENT '单号',
  `warehouse_id` int(11) DEFAULT NULL COMMENT '仓库id',
  `inventory_type` varchar(8) DEFAULT NULL COMMENT '库存变动类型(0-采购入库,1-销售出库,2-调拨入库,3-调拨出库,4-盘盈入库,5-盘亏出库)',
  `sku_id` int(11) DEFAULT NULL COMMENT '商品skuId',
  `price` decimal(20,6) DEFAULT NULL COMMENT '价格',
  `qty` decimal(20,6) DEFAULT NULL COMMENT '库存数量',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='库存操作日志表';

/*Data for the table `wms_inventory_log` */

insert  into `wms_inventory_log`(`id`,`sn`,`warehouse_id`,`inventory_type`,`sku_id`,`price`,`qty`,`del_flag`,`tenant_id`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (7,'PO1628145427548',2,'0',193,NULL,'11.000000','\0',6,'13326778347','2021-08-06 05:10:13','',NULL,NULL),(8,'PO1628145427548',2,'0',194,NULL,'32.000000','\0',6,'13326778347','2021-08-06 05:10:13','',NULL,NULL),(9,'PO1628145427548',2,'0',201,NULL,'55.000000','\0',6,'13326778347','2021-08-06 05:10:13','',NULL,NULL),(10,'PO1628459678837',2,'0',194,'677.000000','11.000000','\0',NULL,'admin','2021-08-06 05:10:13','',NULL,NULL),(11,'PO1628473551351',2,'0',193,'599.000000','10.000000','\0',NULL,'admin','2021-08-06 05:10:13','',NULL,NULL),(12,'PO1628473551351',2,'0',193,'599.000000','20.000000','\0',NULL,'admin','2021-08-06 05:10:13','',NULL,NULL),(13,'PO1628475027059',2,'0',194,'1599.000000','10.000000','\0',NULL,'admin','2021-08-06 05:10:13','',NULL,NULL),(14,'PO1628475027059',2,'0',194,'1599.000000','20.000000','\0',NULL,'admin','2021-08-06 05:10:13','',NULL,NULL),(15,'PO1628475027059',2,'0',195,'2599.000000','30.000000','\0',NULL,'admin','2021-08-09 10:11:42','',NULL,NULL),(16,'SO1628476482152',2,'1',193,'999.000000','10.000000','\0',NULL,'admin','2021-08-06 05:10:13','',NULL,NULL),(17,'SO1628476482152',2,'1',194,'1999.000000','5.000000','\0',NULL,'admin','2021-08-06 05:10:13','',NULL,NULL);

/*Table structure for table `wms_product` */

DROP TABLE IF EXISTS `wms_product`;

CREATE TABLE `wms_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) NOT NULL COMMENT '商品编号',
  `product_code` varchar(64) NOT NULL COMMENT '商品编码',
  `category_id` int(11) DEFAULT NULL COMMENT '商品类别id',
  `is_spec` bit(1) DEFAULT NULL COMMENT '是否多规格',
  `spec_list` varchar(2000) DEFAULT NULL COMMENT '规格参数',
  `product_name` varchar(500) NOT NULL COMMENT '商品名称',
  `unit` varchar(8) DEFAULT NULL COMMENT '商品单位',
  `images` text COMMENT '图片',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='商品信息表';

/*Data for the table `wms_product` */

insert  into `wms_product`(`id`,`sn`,`product_code`,`category_id`,`is_spec`,`spec_list`,`product_name`,`unit`,`images`,`del_flag`,`tenant_id`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (2,'PT1621463010146','efefe',2,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-15 04:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-15 05:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-19 10:10:42\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":2,\"specName\":\"尺寸\",\"specAttr\":[\"大\",\"中\",\"小\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','efefe','7',NULL,'',NULL,'admin','2021-05-20 06:23:30','admin','2021-05-20 14:42:30','efefe'),(3,'PT1621463139460','dsds',0,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-15 04:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-15 05:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-19 10:10:42\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":2,\"specName\":\"尺寸\",\"specAttr\":[\"大\",\"中\",\"小\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','sdsd','7',NULL,'',NULL,'admin','2021-05-20 06:25:39','admin','2021-05-20 14:42:32','sdsd'),(4,'PT1621493040368','P9394999938',2,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-15 04:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-15 05:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-19 10:10:42\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":2,\"specName\":\"尺寸\",\"specAttr\":[\"大\",\"中\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-20 14:09:45\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":3,\"specName\":\"套餐\",\"specAttr\":[\"套餐一\",\"套餐二\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','电饭锅','7',NULL,'',NULL,'admin','2021-05-20 14:44:00','admin','2021-05-21 16:41:54','dldklfdlddsfsfsfsfsfsf'),(5,'PT1621493580404','K9899939488',2,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-15 04:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-15 05:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-19 10:10:42\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":2,\"specName\":\"尺寸\",\"specAttr\":[\"大\",\"中\",\"小\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-20 14:09:45\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":3,\"specName\":\"套餐\",\"specAttr\":[\"套餐一\",\"套餐二\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','手机','7',NULL,'',NULL,'admin','2021-05-20 14:53:00','admin','2021-05-21 16:41:52','llksjfjefjefjlef'),(6,'PT1621494527878','dfdfefef',1,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-15 04:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-15 05:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-19 10:10:42\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":2,\"specName\":\"尺寸\",\"specAttr\":[\"大\",\"中\",\"小\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-20 14:09:45\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":3,\"specName\":\"套餐\",\"specAttr\":[\"套餐一\",\"套餐二\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','wefwefwf','7',NULL,'',NULL,'admin','2021-05-20 15:08:48','admin','2021-05-30 07:07:24','wewfw'),(7,'PT1621498292006','123',1,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-19 02:10:42\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":2,\"specName\":\"尺寸\",\"specAttr\":[\"大\",\"中\",\"小\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-20 06:09:45\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":3,\"specName\":\"套餐\",\"specAttr\":[\"套餐一\",\"套餐二\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','皮干','7',NULL,'',NULL,'admin','2021-05-20 16:11:32','admin','2021-05-21 16:41:56','1111'),(8,'PT1621498445024','111',2,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-19 02:10:42\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":2,\"specName\":\"尺寸\",\"specAttr\":[\"大\",\"中\",\"小\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-20 06:09:45\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":3,\"specName\":\"套餐\",\"specAttr\":[\"套餐一\",\"套餐二\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','vivi','7',NULL,'',NULL,'admin','2021-05-20 16:14:05','admin','2021-05-21 15:02:05','22222'),(9,'PT1621533777671','555',2,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','夜深了','7',NULL,'',NULL,'admin','2021-05-21 02:02:58','admin','2021-05-21 15:02:08','睡觉'),(10,'PT1621576528048','155214',0,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','西瓜','7',NULL,'',NULL,'admin','2021-05-21 13:55:28','admin','2021-05-21 15:02:02','真甜'),(11,'PT1621576605120','111',2,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"specName\":\"形状\",\"specAttr\":[\"1\"],\"inputVisible\":false,\"inputValue\":\"\"}]','南瓜','7',NULL,'',NULL,'admin','2021-05-21 13:56:45','admin','2021-05-21 16:24:52','真大'),(12,'PT1621580561481','139',2,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-19 02:10:42\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":2,\"specName\":\"尺寸\",\"specAttr\":[\"大\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"specName\":\"1\",\"specAttr\":[\"2\",\"3\"],\"inputVisible\":false,\"inputValue\":\"\"}]','西西','7',NULL,'',NULL,'admin','2021-05-21 15:02:41','admin','2021-05-21 15:43:34','真好'),(13,'PT1621583142131','555',2,'','[]','西西','7',NULL,'',NULL,'admin','2021-05-21 15:45:42','admin','2021-05-21 15:53:10','真好'),(14,'PT1621583613500','11',2,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-19 02:10:42\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":2,\"specName\":\"尺寸\",\"specAttr\":[\"大\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','1212','7',NULL,'',NULL,'admin','2021-05-21 15:53:34','admin','2021-05-21 15:57:40','2121212'),(15,'PT1621583883604','12',2,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-19 02:10:42\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":2,\"specName\":\"尺寸\",\"specAttr\":[\"大\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','21212','7',NULL,'',NULL,'admin','2021-05-21 15:58:04','admin','2021-05-21 16:24:50','12121'),(16,'PT1621584305535','155214',2,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-19 02:10:42\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":2,\"specName\":\"尺寸\",\"specAttr\":[\"大\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"specName\":\"1\",\"specAttr\":[\"1\"],\"inputVisible\":false,\"inputValue\":\"\"}]','辣条','7',NULL,'',NULL,'admin','2021-05-21 16:05:06','admin','2021-05-30 07:07:22','真辣'),(17,'PT1621585156469','11',0,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','11','7',NULL,'',NULL,'admin','2021-05-21 16:19:16','admin','2021-05-21 16:24:47','11'),(18,'PT1621585343293','454',0,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','阿萨','7',NULL,'',NULL,'admin','2021-05-21 16:22:23','admin','2021-05-21 16:24:45','啊飒飒'),(19,'PT1621585504475','11',0,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','1','7',NULL,'',NULL,'admin','2021-05-21 16:25:04','admin','2021-05-21 16:38:19','1'),(20,'PT1621585524876','2222',0,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','222','7',NULL,'',NULL,'admin','2021-05-21 16:25:25','admin','2021-05-21 16:38:17','222'),(21,'PT1621586662268','111',2,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"specName\":\"1\",\"specAttr\":[\"2\",\"3\"],\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-19 02:10:42\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":2,\"specName\":\"尺寸\",\"specAttr\":[\"大\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"specName\":\"5\",\"specAttr\":[\"6\"],\"inputVisible\":false,\"inputValue\":\"\"}]','12121','7',NULL,'',NULL,'admin','2021-05-21 16:44:22','admin','2021-05-30 07:07:20','12121212'),(22,'PT1621588859393','1212',0,'','[{\"specName\":\"1\",\"specAttr\":[\"2\",\"3\"],\"inputVisible\":false,\"inputValue\":\"\"},{\"specName\":\"4\",\"specAttr\":[\"5\",\"6\"],\"inputVisible\":false,\"inputValue\":\"\"}]','121','7',NULL,'',NULL,'admin','2021-05-21 17:20:59','admin','2021-05-30 07:07:16','121212'),(23,'PT1621592751862','1212',0,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','212','7',NULL,'',NULL,'admin','2021-05-21 18:25:52','admin','2021-05-21 18:26:19','1212'),(24,'PT1622325971727','1212121212',2,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"粉绝\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-19 02:10:42\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":2,\"specName\":\"尺寸\",\"specAttr\":[\"大\",\"小\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','1212121','7',NULL,'',NULL,'admin','2021-05-30 06:06:12','admin','2021-05-30 07:07:19','wewe'),(25,'PT1622329717155','ekfjefj9992',2,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-19 02:10:42\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":2,\"specName\":\"尺寸\",\"specAttr\":[\"大\",\"小\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','efwef','7',NULL,'\0',NULL,'admin','2021-05-30 07:08:37','',NULL,'sdsdsd'),(32,'PT1626335281681','1111',0,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-14 20:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-14 21:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','test11','7',NULL,'\0',NULL,'test','2021-07-15 15:48:02','',NULL,NULL),(33,'PT1627955186924','Tkkkkjk-002',2,'\0','[]','小米10','7',NULL,'\0',NULL,'admin','2021-08-03 09:46:27','',NULL,'小米10小米10小米10小米10小米10小米10小米10小米10'),(34,'PT1628130763573','SP00001',3,'','[{\"searchValue\":null,\"createBy\":\"13326778347\",\"createTime\":\"2021-08-05 10:24:15\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":4,\"specName\":\"颜色\",\"specAttr\":[\"红\",\"蓝\",\"黑\"],\"delFlag\":false,\"tenantId\":6,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"13326778347\",\"createTime\":\"2021-08-05 10:25:12\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":5,\"specName\":\"尺寸\",\"specAttr\":[\"4.5英寸\",\"5.3英寸\",\"6.0英寸\"],\"delFlag\":false,\"tenantId\":6,\"inputVisible\":false,\"inputValue\":\"\"}]','小米10','7',NULL,'\0',6,'13326778347','2021-08-05 10:32:44','',NULL,'sddsd'),(35,'PT1628240395663','feef',2,'\0','[]','fef','7',NULL,'\0',NULL,'admin','2021-08-06 16:59:56','',NULL,'efe'),(36,'PT1628489442767','fgfgg',2,'\0','[]','gfgfg','6',NULL,'\0',NULL,'admin','2021-08-09 14:10:43','admin','2021-08-09 20:43:38','ffgfg'),(37,'PT1628489894612','wewew',2,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-15 04:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-15 05:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"},{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2021-05-19 10:10:42\",\"updateBy\":\"\",\"updateTime\":null,\"remark\":null,\"params\":{},\"id\":2,\"specName\":\"尺寸\",\"specAttr\":[\"大\",\"小\"],\"delFlag\":false,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','weee','7',NULL,'\0',NULL,'admin','2021-08-09 14:18:15','admin','2021-08-09 20:43:31','wewe'),(38,'PT1628512632084','uuuwjjje',2,'','[{\"searchValue\":null,\"createBy\":\"\",\"createTime\":\"2021-05-15 04:55:54\",\"updateBy\":\"\",\"updateTime\":\"2021-05-15 05:33:22\",\"remark\":null,\"params\":{},\"id\":1,\"specName\":\"颜色\",\"specAttr\":[\"红色\",\"白色\",\"黑色\"],\"delFlag\":null,\"tenantId\":null,\"inputVisible\":false,\"inputValue\":\"\"}]','小米11','7',NULL,'\0',NULL,'admin','2021-08-09 20:37:12','admin','2021-08-09 20:39:31',NULL),(39,'PT1628543131663','ddsssssdd',4,'\0','[]','sdsd','2',NULL,'\0',NULL,'admin','2021-08-10 05:05:32','',NULL,'sdsd');

/*Table structure for table `wms_product_category` */

DROP TABLE IF EXISTS `wms_product_category`;

CREATE TABLE `wms_product_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `category_name` varchar(64) NOT NULL COMMENT '分类名称',
  `parent_id` int(11) DEFAULT NULL COMMENT '父类别id',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(11) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='商品分类信息表';

/*Data for the table `wms_product_category` */

insert  into `wms_product_category`(`id`,`category_name`,`parent_id`,`del_flag`,`tenant_id`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (1,'手机',0,NULL,NULL,'','2021-05-14 16:04:22','','2021-05-30 07:07:54',NULL),(2,'小米',1,NULL,NULL,'','2021-05-14 16:04:29','','2021-05-30 07:08:00',NULL),(3,'手机',0,NULL,6,'','2021-08-05 10:23:49','',NULL,NULL),(4,'ddd',0,NULL,NULL,'','2021-08-10 05:05:02','',NULL,NULL),(5,'游戏机',0,NULL,NULL,'','2021-08-14 05:21:32','',NULL,NULL);

/*Table structure for table `wms_product_sku` */

DROP TABLE IF EXISTS `wms_product_sku`;

CREATE TABLE `wms_product_sku` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) DEFAULT NULL COMMENT '商品编号',
  `bar_code` varchar(64) DEFAULT NULL COMMENT '商品条码',
  `sku_name` varchar(500) DEFAULT NULL COMMENT '商品sku名称',
  `specifications` varchar(500) DEFAULT NULL COMMENT '规格',
  `product_id` int(11) DEFAULT NULL COMMENT '商品id',
  `sale_price` decimal(10,6) DEFAULT NULL COMMENT '零售价',
  `trade_price` decimal(10,6) DEFAULT NULL COMMENT '批发价',
  `vip_price` decimal(10,6) DEFAULT NULL COMMENT '会员价',
  `discount1` decimal(10,6) DEFAULT NULL COMMENT '折扣一',
  `discount2` decimal(10,6) DEFAULT NULL COMMENT '折扣二',
  `purchase_price` decimal(10,6) DEFAULT NULL COMMENT '预计采购价',
  `images` text COMMENT '图片',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(11) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=220 DEFAULT CHARSET=utf8 COMMENT='商品sku信息表';

/*Data for the table `wms_product_sku` */

insert  into `wms_product_sku`(`id`,`sn`,`bar_code`,`sku_name`,`specifications`,`product_id`,`sale_price`,`trade_price`,`vip_price`,`discount1`,`discount2`,`purchase_price`,`images`,`del_flag`,`tenant_id`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (1,'ST1621463010158',NULL,NULL,'红色',2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-20 06:23:30','',NULL,NULL),(2,'ST1621463010220',NULL,NULL,'白色',2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-20 06:23:30','',NULL,NULL),(3,'ST1621463010231',NULL,NULL,'黑色',2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-20 06:23:30','',NULL,NULL),(4,'ST1621463139476',NULL,NULL,'红色_大',3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-20 06:25:39','',NULL,NULL),(5,'ST1621463139491',NULL,NULL,'红色_中',3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-20 06:25:39','',NULL,NULL),(6,'ST1621463139507',NULL,NULL,'红色_小',3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-20 06:25:39','',NULL,NULL),(7,'ST1621463139520',NULL,NULL,'白色_大',3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-20 06:25:39','',NULL,NULL),(8,'ST1621463139532',NULL,NULL,'白色_中',3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-20 06:25:39','',NULL,NULL),(9,'ST1621463139547',NULL,NULL,'白色_小',3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-20 06:25:39','',NULL,NULL),(10,'ST1621463139558',NULL,NULL,'黑色_大',3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-20 06:25:39','',NULL,NULL),(11,'ST1621463139568',NULL,NULL,'黑色_中',3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-20 06:25:39','',NULL,NULL),(12,'ST1621463139577',NULL,NULL,'黑色_小',3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-20 06:25:39','',NULL,NULL),(13,'ST1621493040393','33344',NULL,'红色_大',4,'4.000000','4.000000','4.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-20 14:44:00','',NULL,NULL),(14,'ST1621493040410','434343',NULL,'红色_中',4,'6.000000','6.000000','6.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-20 14:44:00','',NULL,NULL),(15,'ST1621493040420','43434',NULL,'黑色_大',4,'7.000000','7.000000','7.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-20 14:44:00','',NULL,NULL),(16,'ST1621493040429','343434',NULL,'黑色_中',4,'5.000000','5.000000','5.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-20 14:44:00','',NULL,NULL),(17,'ST1621493580415',NULL,NULL,'红色',5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-20 14:53:00','',NULL,NULL),(18,'ST1621493580424',NULL,NULL,'白色',5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-20 14:53:00','',NULL,NULL),(19,'ST1621493580431',NULL,NULL,'黑色',5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-20 14:53:00','',NULL,NULL),(20,'ST1621494527887',NULL,NULL,'红色',6,NULL,NULL,NULL,NULL,NULL,NULL,'http://8.135.119.210:8080/file/statics/2021/05/23/845a6f41-b8ad-4331-bfd1-bd2500f8b5fd.jpg','\0',NULL,'admin','2021-05-20 15:08:48','',NULL,NULL),(21,'ST1621494527898',NULL,NULL,'白色',6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-20 15:08:48','',NULL,NULL),(22,'ST1621498292051','1',NULL,'红色',7,'1.000000','1.000000','1.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-20 16:11:32','',NULL,NULL),(23,'ST1621498292077',NULL,NULL,'白色',7,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-20 16:11:32','',NULL,NULL),(24,'ST1621498292093',NULL,NULL,'黑色',7,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-20 16:11:32','',NULL,NULL),(25,'ST1621498445043','1',NULL,'红色',8,'1.000000','1.000000','1.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-20 16:14:05','',NULL,NULL),(26,'ST1621498445051',NULL,NULL,'白色',8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-20 16:14:05','',NULL,NULL),(27,'ST1621498445058',NULL,NULL,'黑色',8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-20 16:14:05','',NULL,NULL),(28,'ST1621533777676','1',NULL,'红色_大',9,'1.000000','1.000000','1.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 02:02:58','',NULL,NULL),(29,'ST1621533777692',NULL,NULL,'红色_中',9,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 02:02:58','',NULL,NULL),(30,'ST1621533777701',NULL,NULL,'红色_小',9,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 02:02:58','',NULL,NULL),(31,'ST1621533872405','1',NULL,'红色',9,'1.000000','1.000000','1.000000',NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 02:04:32','',NULL,NULL),(32,'ST1621533872416',NULL,NULL,'白色',9,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 02:04:32','',NULL,NULL),(33,'ST1621533872424',NULL,NULL,'黑色',9,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 02:04:32','',NULL,NULL),(34,'ST1621576528055','1',NULL,'红色',10,'13.000000','3.000000','4.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 13:55:28','',NULL,NULL),(35,'ST1621576528059',NULL,NULL,'白色',10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 13:55:28','',NULL,NULL),(36,'ST1621576528066',NULL,NULL,'黑色',10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 13:55:28','',NULL,NULL),(37,'ST1621576605128','12',NULL,'红色_1',11,'12.000000','12.000000','12.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 13:56:45','',NULL,NULL),(38,'ST1621576605135',NULL,NULL,'白色_1',11,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 13:56:45','',NULL,NULL),(39,'ST1621576605144',NULL,NULL,'黑色_1',11,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 13:56:45','',NULL,NULL),(40,'ST1621580561514','12',NULL,'红色',12,'12.000000','12.000000','12.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 15:02:41','',NULL,NULL),(41,'ST1621580561528',NULL,NULL,'白色',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 15:02:41','',NULL,NULL),(42,'ST1621580561541',NULL,NULL,'黑色',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 15:02:41','',NULL,NULL),(43,'ST1621581170592','13',NULL,'红色_大',12,'14.000000','14.000000','14.000000',NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:12:51','',NULL,NULL),(44,'ST1621581170600',NULL,NULL,'白色_大',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:12:51','',NULL,NULL),(45,'ST1621581170610',NULL,NULL,'黑色_大',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:12:51','',NULL,NULL),(46,'ST1621581205616','12',NULL,'红色_大_2',12,'12.000000','12.000000','12.000000',NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:13:26','',NULL,NULL),(47,'ST1621581205624',NULL,NULL,'红色_大_3',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:13:26','',NULL,NULL),(48,'ST1621581205632',NULL,NULL,'白色_大_2',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:13:26','',NULL,NULL),(49,'ST1621581205637',NULL,NULL,'白色_大_3',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:13:26','',NULL,NULL),(50,'ST1621581205641',NULL,NULL,'黑色_大_2',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:13:26','',NULL,NULL),(51,'ST1621581205652',NULL,NULL,'黑色_大_3',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:13:26','',NULL,NULL),(52,'ST1621583142138','12',NULL,'红色',13,'23.000000','23.000000','23.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 15:45:42','',NULL,NULL),(53,'ST1621583142146',NULL,NULL,'白色',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 15:45:42','',NULL,NULL),(54,'ST1621583142154',NULL,NULL,'黑色',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 15:45:42','',NULL,NULL),(55,'ST1621583339751','1',NULL,'红色_大',13,'1.000000','1.000000','1.000000',NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:49:00','',NULL,NULL),(56,'ST1621583339758',NULL,NULL,'红色_中',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:49:00','',NULL,NULL),(57,'ST1621583339763',NULL,NULL,'红色_小',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:49:00','',NULL,NULL),(58,'ST1621583339772',NULL,NULL,'白色_大',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:49:00','',NULL,NULL),(59,'ST1621583339776',NULL,NULL,'白色_中',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:49:00','',NULL,NULL),(60,'ST1621583339781',NULL,NULL,'白色_小',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:49:00','',NULL,NULL),(61,'ST1621583339792',NULL,NULL,'黑色_大',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:49:00','',NULL,NULL),(62,'ST1621583339799',NULL,NULL,'黑色_中',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:49:00','',NULL,NULL),(63,'ST1621583339804',NULL,NULL,'黑色_小',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:49:00','',NULL,NULL),(64,'ST1621583417224','1',NULL,'红色',13,'1.000000','1.000000','1.000000',NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:50:17','',NULL,NULL),(65,'ST1621583417230',NULL,NULL,'白色',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:50:17','',NULL,NULL),(66,'ST1621583417236',NULL,NULL,'黑色',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:50:17','',NULL,NULL),(67,'ST1621583613508','12',NULL,'红色',14,'12.000000','12.000000','12.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 15:53:34','admin','2021-05-21 15:53:52','2121212'),(68,'ST1621583613515',NULL,NULL,'白色',14,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 15:53:34','admin','2021-05-21 15:53:52','2121212'),(69,'ST1621583613519',NULL,NULL,'黑色',14,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 15:53:34','admin','2021-05-21 15:53:52','2121212'),(70,'ST1621583669683','1',NULL,'红色_大',14,'1.000000','1.000000','1.000000',NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:54:30','',NULL,NULL),(71,'ST1621583669688',NULL,NULL,'白色_大',14,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:54:30','',NULL,NULL),(72,'ST1621583669695',NULL,NULL,'黑色_大',14,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:54:30','',NULL,NULL),(73,'ST1621583883610','1',NULL,'红色',15,'1.000000','1.000000','1.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 15:58:04','',NULL,NULL),(74,'ST1621583883618',NULL,NULL,'白色',15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 15:58:04','',NULL,NULL),(75,'ST1621583883629',NULL,NULL,'黑色',15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 15:58:04','',NULL,NULL),(76,'ST1621583926072','12',NULL,'红色_大',15,'12.000000','12.000000','12.000000',NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:58:46','',NULL,NULL),(77,'ST1621583926078',NULL,NULL,'白色_大',15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:58:46','',NULL,NULL),(78,'ST1621583926083',NULL,NULL,'黑色_大',15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2021-05-21 15:58:46','',NULL,NULL),(79,'ST1621584305543','1',NULL,'红色',16,'1.000000','1.000000','1.000000',NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:05:06','admin','2021-05-21 16:43:12',NULL),(80,'ST1621584305554',NULL,NULL,'白色',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:05:06','admin','2021-05-21 16:43:12',NULL),(81,'ST1621584305569',NULL,NULL,'黑色',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:05:06','admin','2021-05-21 16:43:12',NULL),(82,'ST1621585133524','12',NULL,'红色_大',16,'12.000000','12.000000','12.000000',NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:18:54','admin','2021-05-21 16:43:12',NULL),(83,'ST1621585133531',NULL,NULL,'白色_大',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:18:54','admin','2021-05-21 16:43:12',NULL),(84,'ST1621585133539',NULL,NULL,'黑色_大',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:18:54','admin','2021-05-21 16:43:12',NULL),(85,'ST1621585156479',NULL,NULL,'红色',17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 16:19:16','',NULL,NULL),(86,'ST1621585156487',NULL,NULL,'白色',17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 16:19:16','',NULL,NULL),(87,'ST1621585156497',NULL,NULL,'黑色',17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 16:19:16','',NULL,NULL),(88,'ST1621585343297',NULL,NULL,'红色',18,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 16:22:23','',NULL,NULL),(89,'ST1621585343301',NULL,NULL,'白色',18,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 16:22:23','',NULL,NULL),(90,'ST1621585343306',NULL,NULL,'黑色',18,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 16:22:23','',NULL,NULL),(91,'ST1621585504492',NULL,NULL,'红色',19,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 16:25:04','',NULL,NULL),(92,'ST1621585504506',NULL,NULL,'白色',19,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 16:25:04','',NULL,NULL),(93,'ST1621585504520',NULL,NULL,'黑色',19,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 16:25:04','',NULL,NULL),(94,'ST1621585524883',NULL,NULL,'红色',20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 16:25:25','',NULL,NULL),(95,'ST1621585524887',NULL,NULL,'白色',20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 16:25:25','',NULL,NULL),(96,'ST1621585524902',NULL,NULL,'黑色',20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 16:25:25','',NULL,NULL),(97,'ST1621586239418','1',NULL,'红色',16,'1.000000','1.000000','1.000000',NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:37:19','admin','2021-05-21 16:43:12',NULL),(98,'ST1621586239437',NULL,NULL,'白色',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:37:19','admin','2021-05-21 16:43:12',NULL),(99,'ST1621586239444',NULL,NULL,'黑色',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:37:19','admin','2021-05-21 16:43:12',NULL),(100,'ST1621586263360','1',NULL,'红色_大',16,'1.000000','1.000000','1.000000',NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:37:43','admin','2021-05-21 16:43:12',NULL),(101,'ST1621586263368',NULL,NULL,'白色_大',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:37:43','admin','2021-05-21 16:43:12',NULL),(102,'ST1621586263377',NULL,NULL,'黑色_大',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:37:43','admin','2021-05-21 16:43:12',NULL),(103,'ST1621586551500','12',NULL,'红色_大_方',16,'12.000000','12.000000','12.000000',NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:42:31','admin','2021-05-21 16:43:12',NULL),(104,'ST1621586551517',NULL,NULL,'红色_大_圆',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:42:31','admin','2021-05-21 16:43:12',NULL),(105,'ST1621586551525',NULL,NULL,'白色_大_方',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:42:31','admin','2021-05-21 16:43:12',NULL),(106,'ST1621586551540',NULL,NULL,'白色_大_圆',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:42:31','admin','2021-05-21 16:43:12',NULL),(107,'ST1621586551549',NULL,NULL,'黑色_大_方',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:42:31','admin','2021-05-21 16:43:12',NULL),(108,'ST1621586551553',NULL,NULL,'黑色_大_圆',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:42:31','admin','2021-05-21 16:43:12',NULL),(109,'ST1621586592098','12',NULL,'红色_大_1',16,'13.000000','13.000000','13.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 16:43:12','',NULL,NULL),(110,'ST1621586592107',NULL,NULL,'白色_大_1',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 16:43:12','',NULL,NULL),(111,'ST1621586592117',NULL,NULL,'黑色_大_1',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 16:43:12','',NULL,NULL),(112,'ST1621586662278','12',NULL,'红色',21,'12.000000','12.000000','12.000000',NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:44:22','admin','2021-05-21 18:27:55',NULL),(113,'ST1621586662288',NULL,NULL,'白色',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:44:22','admin','2021-05-21 18:27:55',NULL),(114,'ST1621586662296',NULL,NULL,'黑色',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 16:44:22','admin','2021-05-21 18:27:55',NULL),(115,'ST1621588560525','12',NULL,'',21,'10.000000','10.000000','10.000000','0.000000',NULL,'0.000000','','',NULL,'admin','2021-05-21 17:16:01','admin','2021-05-21 18:27:55',''),(116,'ST1621588636055','12',NULL,'红色',21,'12.000000','12.000000','12.000000',NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:17:16','admin','2021-05-21 18:27:55',NULL),(117,'ST1621588636065',NULL,NULL,'白色',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:17:16','admin','2021-05-21 18:27:55',NULL),(118,'ST1621588636070',NULL,NULL,'黑色',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:17:16','admin','2021-05-21 18:27:55',NULL),(119,'ST1621588650801','12',NULL,'',21,'10.000000','10.000000','10.000000','0.000000',NULL,'0.000000','','',NULL,'admin','2021-05-21 17:17:31','admin','2021-05-21 18:27:55',''),(120,'ST1621588826492','12',NULL,'',21,'10.000000','10.000000','10.000000','0.000000',NULL,NULL,'','',NULL,'admin','2021-05-21 17:20:26','admin','2021-05-21 18:27:55','12121212'),(121,'ST1621588831406','12',NULL,'',21,'10.000000','10.000000','10.000000','0.000000',NULL,NULL,'','',NULL,'admin','2021-05-21 17:20:31','admin','2021-05-21 18:27:55','12121212'),(122,'ST1621588859398','12',NULL,'',22,'10.000000','10.000000','10.000000','0.000000',NULL,'0.000000','','',NULL,'admin','2021-05-21 17:20:59','admin','2021-05-21 18:28:09',''),(123,'ST1621588869561','12',NULL,'',22,'10.000000','10.000000','100.000000','0.000000',NULL,NULL,'','',NULL,'admin','2021-05-21 17:21:10','admin','2021-05-21 18:28:09','121212'),(124,'ST1621588956835','1',NULL,'2_5',22,'1.000000','12.000000','12.000000',NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:22:37','admin','2021-05-21 18:28:09',NULL),(125,'ST1621588956848',NULL,NULL,'2_6',22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:22:37','admin','2021-05-21 18:28:09',NULL),(126,'ST1621588956853',NULL,NULL,'3_5',22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:22:37','admin','2021-05-21 18:28:09',NULL),(127,'ST1621588956857',NULL,NULL,'3_6',22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:22:37','admin','2021-05-21 18:28:09',NULL),(128,'ST1621590089737',NULL,NULL,'红色_1',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:41:30','admin','2021-05-21 18:27:55',NULL),(129,'ST1621590089746',NULL,NULL,'红色_2',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:41:30','admin','2021-05-21 18:27:55',NULL),(130,'ST1621590089753',NULL,NULL,'白色_1',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:41:30','admin','2021-05-21 18:27:55',NULL),(131,'ST1621590089771',NULL,NULL,'白色_2',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:41:30','admin','2021-05-21 18:27:55',NULL),(132,'ST1621590089784',NULL,NULL,'黑色_1',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:41:30','admin','2021-05-21 18:27:55',NULL),(133,'ST1621590089791',NULL,NULL,'黑色_2',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:41:30','admin','2021-05-21 18:27:55',NULL),(134,'ST1621590155333',NULL,NULL,'红色',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:42:35','admin','2021-05-21 18:27:55',NULL),(135,'ST1621590155338',NULL,NULL,'白色',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:42:35','admin','2021-05-21 18:27:55',NULL),(136,'ST1621590155345',NULL,NULL,'黑色',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:42:35','admin','2021-05-21 18:27:55',NULL),(137,'ST1621590171672',NULL,NULL,'红色_2',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:42:52','admin','2021-05-21 18:27:55',NULL),(138,'ST1621590171678',NULL,NULL,'红色_3',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:42:52','admin','2021-05-21 18:27:55',NULL),(139,'ST1621590171687',NULL,NULL,'白色_2',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:42:52','admin','2021-05-21 18:27:55',NULL),(140,'ST1621590171691',NULL,NULL,'白色_3',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:42:52','admin','2021-05-21 18:27:55',NULL),(141,'ST1621590171705',NULL,NULL,'黑色_2',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:42:52','admin','2021-05-21 18:27:55',NULL),(142,'ST1621590171713',NULL,NULL,'黑色_3',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 17:42:52','admin','2021-05-21 18:27:55',NULL),(143,'ST1621591364200',NULL,NULL,'红色_2_1',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:02:44','admin','2021-05-21 18:27:55',NULL),(144,'ST1621591364204',NULL,NULL,'红色_3_1',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:02:44','admin','2021-05-21 18:27:55',NULL),(145,'ST1621591364211',NULL,NULL,'白色_2_1',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:02:44','admin','2021-05-21 18:27:55',NULL),(146,'ST1621591364224',NULL,NULL,'白色_3_1',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:02:44','admin','2021-05-21 18:27:55',NULL),(147,'ST1621591364232',NULL,NULL,'黑色_2_1',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:02:44','admin','2021-05-21 18:27:55',NULL),(148,'ST1621591364245',NULL,NULL,'黑色_3_1',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:02:44','admin','2021-05-21 18:27:55',NULL),(149,'ST1621591636007',NULL,NULL,'红色_2',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:07:16','admin','2021-05-21 18:27:55',NULL),(150,'ST1621591636011',NULL,NULL,'红色_3',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:07:16','admin','2021-05-21 18:27:55',NULL),(151,'ST1621591636026',NULL,NULL,'白色_2',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:07:16','admin','2021-05-21 18:27:55',NULL),(152,'ST1621591636039',NULL,NULL,'白色_3',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:07:16','admin','2021-05-21 18:27:55',NULL),(153,'ST1621591636042',NULL,NULL,'黑色_2',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:07:16','admin','2021-05-21 18:27:55',NULL),(154,'ST1621591636046',NULL,NULL,'黑色_3',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:07:16','admin','2021-05-21 18:27:55',NULL),(155,'ST1621592054378',NULL,NULL,'红色_2_大_6',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:14:14','admin','2021-05-21 18:27:55',NULL),(156,'ST1621592054383',NULL,NULL,'红色_3_大_6',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:14:14','admin','2021-05-21 18:27:55',NULL),(157,'ST1621592054386',NULL,NULL,'白色_2_大_6',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:14:14','admin','2021-05-21 18:27:55',NULL),(158,'ST1621592054389',NULL,NULL,'白色_3_大_6',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:14:14','admin','2021-05-21 18:27:55',NULL),(159,'ST1621592054392',NULL,NULL,'黑色_2_大_6',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:14:14','admin','2021-05-21 18:27:55',NULL),(160,'ST1621592054396',NULL,NULL,'黑色_3_大_6',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:14:14','admin','2021-05-21 18:27:55',NULL),(161,'ST1621592540285','1',NULL,'红色_2_大_6',21,'1.000000','1.000000','1.000000',NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:22:20','admin','2021-05-21 18:27:55','12121212'),(162,'ST1621592540291',NULL,NULL,'红色_3_大_6',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:22:20','admin','2021-05-21 18:27:55','12121212'),(163,'ST1621592540295',NULL,NULL,'白色_2_大_6',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:22:20','admin','2021-05-21 18:27:55','12121212'),(164,'ST1621592540299',NULL,NULL,'白色_3_大_6',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:22:20','admin','2021-05-21 18:27:55','12121212'),(165,'ST1621592540302',NULL,NULL,'黑色_2_大_6',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:22:20','admin','2021-05-21 18:27:55','12121212'),(166,'ST1621592540305',NULL,NULL,'黑色_3_大_6',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'admin','2021-05-21 18:22:20','admin','2021-05-21 18:27:55','12121212'),(167,'ST1621592751867','12',NULL,'',23,'10.000000','10.000000','10.000000','0.000000',NULL,'0.000000','','',NULL,'admin','2021-05-21 18:25:52','admin','2021-05-21 18:26:10',''),(168,'ST1621592769607','12',NULL,'红色',23,'12.000000','12.000000','12.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 18:26:10','',NULL,NULL),(169,'ST1621592769611',NULL,NULL,'白色',23,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 18:26:10','',NULL,NULL),(170,'ST1621592769615',NULL,NULL,'黑色',23,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 18:26:10','',NULL,NULL),(171,'ST1621592875072','12',NULL,'红色_2_大_6',21,'12.000000','12.000000','12.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 18:27:55','',NULL,NULL),(172,'ST1621592875088',NULL,NULL,'红色_3_大_6',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 18:27:55','',NULL,NULL),(173,'ST1621592875100',NULL,NULL,'白色_2_大_6',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 18:27:55','',NULL,NULL),(174,'ST1621592875106',NULL,NULL,'白色_3_大_6',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 18:27:55','',NULL,NULL),(175,'ST1621592875109',NULL,NULL,'黑色_2_大_6',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 18:27:55','',NULL,NULL),(176,'ST1621592875112',NULL,NULL,'黑色_3_大_6',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 18:27:55','',NULL,NULL),(177,'ST1621592888580','12',NULL,'2_5',22,'123.000000','12.000000','12.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 18:28:09','',NULL,'121212'),(178,'ST1621592888583',NULL,NULL,'2_6',22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 18:28:09','',NULL,'121212'),(179,'ST1621592888587',NULL,NULL,'3_5',22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 18:28:09','',NULL,'121212'),(180,'ST1621592888591',NULL,NULL,'3_6',22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-21 18:28:09','',NULL,'121212'),(181,'ST1622325971749','s33333',NULL,'红色_大',24,'1.000000','1.000000','1.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-30 06:06:12','',NULL,NULL),(182,'ST1622325971760','223333',NULL,'红色_小',24,'1.000000','1.000000','1.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-30 06:06:12','',NULL,NULL),(183,'ST1622325971767','3333',NULL,'粉绝_大',24,'1.000000','1.000000','1.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-30 06:06:12','',NULL,NULL),(184,'ST1622325971774','33333',NULL,'粉绝_小',24,'1.000000','1.000000','1.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-30 06:06:12','',NULL,NULL),(185,'ST1622329717162','2232',NULL,'红色_大',25,'3.000000','3.000000','3.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-30 07:08:37','',NULL,NULL),(186,'ST1622329717169','2323',NULL,'红色_小',25,'3.000000','3.000000','3.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-30 07:08:37','',NULL,NULL),(187,'ST1622329717173','2323',NULL,'黑色_大',25,'3.000000','3.000000','32.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-30 07:08:37','',NULL,NULL),(188,'ST1622329717177','323',NULL,'黑色_小',25,'3.000000','3.000000','3.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-05-30 07:08:37','',NULL,NULL),(189,'ST1626335281687','1111',NULL,'红色',32,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'test','2021-07-15 15:48:02','',NULL,NULL),(190,'ST1626335281695','2222',NULL,'白色',32,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'test','2021-07-15 15:48:02','',NULL,NULL),(191,'ST1626335281699','3333',NULL,'黑色',32,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,'test','2021-07-15 15:48:02','',NULL,NULL),(192,'ST1627955187126','T0001',NULL,'',33,'3300.000000','3200.000000','3100.000000','0.000000',NULL,'0.000000','','\0',NULL,'admin','2021-08-03 09:46:27','',NULL,''),(193,'ST1628130763917','SP000011',NULL,'红_4.5英寸',34,'999.000000','899.000000','959.000000',NULL,NULL,NULL,NULL,'\0',6,'13326778347','2021-08-05 10:32:44','',NULL,NULL),(194,'ST1628130763941','SP000021',NULL,'红_5.3英寸',34,'1999.000000','1899.000000','1959.000000',NULL,NULL,NULL,NULL,'\0',6,'13326778347','2021-08-05 10:32:44','',NULL,NULL),(195,'ST1628130763958','SP000031',NULL,'红_6.0英寸',34,'2999.000000','2899.000000','2959.000000',NULL,NULL,NULL,NULL,'\0',6,'13326778347','2021-08-05 10:32:44','',NULL,NULL),(196,'ST1628130763974','SP000012',NULL,'蓝_4.5英寸',34,'999.000000','899.000000','959.000000',NULL,NULL,NULL,NULL,'\0',6,'13326778347','2021-08-05 10:32:44','',NULL,NULL),(197,'ST1628130763997','SP000022',NULL,'蓝_5.3英寸',34,'1999.000000','1899.000000','1959.000000',NULL,NULL,NULL,NULL,'\0',6,'13326778347','2021-08-05 10:32:44','',NULL,NULL),(198,'ST1628130764014','SP000032',NULL,'蓝_6.0英寸',34,'2999.000000','2899.000000','2959.000000',NULL,NULL,NULL,NULL,'\0',6,'13326778347','2021-08-05 10:32:44','',NULL,NULL),(199,'ST1628130764031','SP000013',NULL,'黑_4.5英寸',34,'999.000000','899.000000','959.000000',NULL,NULL,NULL,NULL,'\0',6,'13326778347','2021-08-05 10:32:44','',NULL,NULL),(200,'ST1628130764053','SP000023',NULL,'黑_5.3英寸',34,'1999.000000','1899.000000','1959.000000',NULL,NULL,NULL,NULL,'\0',6,'13326778347','2021-08-05 10:32:44','',NULL,NULL),(201,'ST1628130764069','SP000033',NULL,'黑_6.0英寸',34,'2999.000000','2899.000000','2959.000000',NULL,NULL,NULL,NULL,'\0',6,'13326778347','2021-08-05 10:32:44','',NULL,NULL),(202,'ST1628240395737','efe',NULL,'',35,'23.000000','32.000000','32.000000','0.000000',NULL,'0.000000','','\0',NULL,'admin','2021-08-06 16:59:56','',NULL,''),(203,'ST1628489442831','fgfgf',NULL,'fgfg',36,'44.000000','33.000000','34.000000','0.000000',NULL,'0.000000','','',NULL,'admin','2021-08-09 14:10:43','admin','2021-08-09 20:43:38',''),(204,'ST1628489894629','wwe',NULL,'红色_大',37,'322.000000','3.000000','3.000000',NULL,NULL,NULL,NULL,'',NULL,'admin','2021-08-09 14:18:15','admin','2021-08-09 20:43:31',NULL),(205,'ST1628489894642','wewe',NULL,'红色_小',37,'2.000000','3.000000','2.000000',NULL,NULL,NULL,NULL,'',NULL,'admin','2021-08-09 14:18:15','admin','2021-08-09 20:43:31',NULL),(206,'ST1628489894654','wewe',NULL,'黑色_大',37,'2.000000','3.000000','3.000000',NULL,NULL,NULL,NULL,'',NULL,'admin','2021-08-09 14:18:15','admin','2021-08-09 20:43:31',NULL),(207,'ST1628489894665','wew',NULL,'黑色_小',37,'2.000000','3.000000','2.000000',NULL,NULL,NULL,NULL,'',NULL,'admin','2021-08-09 14:18:15','admin','2021-08-09 20:43:31',NULL),(208,'ST1628512632203','fdfe',NULL,'红色',38,'3.000000','3.000000','3.000000',NULL,NULL,NULL,NULL,'',NULL,'admin','2021-08-09 20:37:12','admin','2021-08-09 20:39:31',NULL),(209,'ST1628512632408','rer',NULL,'白色',38,'3.000000','3.000000','3.000000',NULL,NULL,NULL,NULL,'',NULL,'admin','2021-08-09 20:37:12','admin','2021-08-09 20:39:31',NULL),(210,'ST1628512632420','ewr',NULL,'黑色',38,'3.000000','3.000000','3.000000',NULL,NULL,NULL,NULL,'',NULL,'admin','2021-08-09 20:37:12','admin','2021-08-09 20:39:31',NULL),(211,'ST1628512771375','fdfe',NULL,'红色',38,'3.000000','3.000000','3.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-08-09 20:39:31','',NULL,NULL),(212,'ST1628512771388','rer',NULL,'白色',38,'3.000000','3.000000','3.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-08-09 20:39:31','',NULL,NULL),(213,'ST1628512771401','ewr',NULL,'黑色',38,'3.000000','3.000000','3.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-08-09 20:39:31','',NULL,NULL),(214,'ST1628513011132','wwe',NULL,'红色_大',37,'322.000000','3.000000','3.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-08-09 20:43:31','',NULL,'wewe'),(215,'ST1628513011181','wewe',NULL,'红色_小',37,'2.000000','3.000000','2.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-08-09 20:43:31','',NULL,'wewe'),(216,'ST1628513011194','wewe',NULL,'黑色_大',37,'2.000000','3.000000','3.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-08-09 20:43:31','',NULL,'wewe'),(217,'ST1628513011211','wew',NULL,'黑色_小',37,'2.000000','3.000000','2.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-08-09 20:43:31','',NULL,'wewe'),(218,'ST1628513018333','fgfgf',NULL,'fgfg',36,'44.000000','33.000000','34.000000','0.000000',NULL,NULL,'','\0',NULL,'admin','2021-08-09 20:43:38','',NULL,'ffgfg'),(219,'ST1628543131683','sds',NULL,'sdsd',39,'3.000000','3.000000','3.000000','0.000000',NULL,'0.000000','','\0',NULL,'admin','2021-08-10 05:05:32','',NULL,'');

/*Table structure for table `wms_product_spec` */

DROP TABLE IF EXISTS `wms_product_spec`;

CREATE TABLE `wms_product_spec` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `spec_name` varchar(30) NOT NULL COMMENT '规格名称',
  `spec_attr` varchar(1000) NOT NULL COMMENT '规格属性',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='商品规格表';

/*Data for the table `wms_product_spec` */

insert  into `wms_product_spec`(`id`,`spec_name`,`spec_attr`,`del_flag`,`tenant_id`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (1,'颜色','[\"红色\",\"白色\",\"黑色\"]',NULL,NULL,'','2021-05-15 04:55:54','','2021-05-15 05:33:22',NULL),(2,'尺寸','[\"大\",\"中\",\"小\"]','\0',NULL,'admin','2021-05-19 10:10:42','',NULL,NULL),(3,'套餐','[\"套餐一\",\"套餐二\",\"套餐三\"]','\0',NULL,'admin','2021-05-20 14:09:45','test','2021-05-31 15:58:11',NULL),(4,'颜色','[\"红\",\"蓝\",\"黑\"]','\0',6,'13326778347','2021-08-05 10:24:15','',NULL,NULL),(5,'尺寸','[\"4.5英寸\",\"5.3英寸\",\"6.0英寸\"]','\0',6,'13326778347','2021-08-05 10:25:12','',NULL,NULL);

/*Table structure for table `wms_purchase_order` */

DROP TABLE IF EXISTS `wms_purchase_order`;

CREATE TABLE `wms_purchase_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) NOT NULL COMMENT '采购订单编号',
  `supplier_id` int(11) DEFAULT NULL COMMENT '供应商id',
  `buy_date` datetime DEFAULT NULL COMMENT '购货日期',
  `bill_date` datetime DEFAULT NULL COMMENT '单据日期',
  `inventory_type` varchar(8) DEFAULT NULL COMMENT '0-采购入库,6-采购退货',
  `inventory_status` char(1) DEFAULT NULL COMMENT '0-未确认,1-确认入库,2-确认出库',
  `discount_rate` decimal(20,6) DEFAULT NULL COMMENT '优惠率',
  `discount_amount` decimal(20,6) DEFAULT NULL COMMENT '优惠金额',
  `total_amount` decimal(20,6) DEFAULT NULL COMMENT '合计金额',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='采购订单表';

/*Data for the table `wms_purchase_order` */

insert  into `wms_purchase_order`(`id`,`sn`,`supplier_id`,`buy_date`,`bill_date`,`inventory_type`,`inventory_status`,`discount_rate`,`discount_amount`,`total_amount`,`del_flag`,`tenant_id`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (1,'PO1628127164930',1,'2021-08-05 00:00:00','2021-08-07 00:00:00','0','0','33.000000','233.000000','2332.000000','',NULL,'admin','2021-08-05 09:32:45','admin','2021-08-05 15:54:09',NULL),(2,'PO1628145072278',6,'2021-08-05 00:00:00','2021-08-05 00:00:00','0','0','5.000000','55.000000','555.000000','',6,'13326778347','2021-08-05 14:31:12','admin','2021-08-05 15:54:11','反反复复烦烦烦'),(3,'PO1628145427548',6,'2021-08-05 00:00:00','2021-08-05 00:00:00','0','1','6.000000','66.000000','226182.000000','\0',6,'13326778347','2021-08-05 14:37:08','admin','2021-08-09 05:50:37','同一天-'),(4,'PO1628459678837',6,'2021-08-09 00:00:00','2021-08-09 00:00:00','0','1',NULL,NULL,'7447.000000','\0',NULL,'admin','2021-08-09 05:54:39','admin','2021-08-09 06:02:36','eef'),(5,'PO1628473551351',6,'2021-08-09 00:00:00','2021-08-09 00:00:00','0','1',NULL,NULL,'17970.000000','\0',NULL,'admin','2021-08-09 09:45:51','',NULL,'tththth'),(6,'PO1628475027059',3,'2021-08-09 00:00:00','2021-08-09 00:00:00','0','1',NULL,NULL,'125940.000000','\0',NULL,'admin','2021-08-09 10:10:27','',NULL,'dfef'),(7,'PO1628475869463',6,'2021-08-09 00:00:00','2021-08-09 00:00:00','0','0',NULL,NULL,'15990.000000','\0',NULL,'admin','2021-08-09 10:24:29','',NULL,'reer'),(8,'PO1628543363833',6,'2021-08-10 00:00:00','2021-08-10 00:00:00','0','0',NULL,NULL,'7659.000000','\0',NULL,'admin','2021-08-10 05:09:24','',NULL,'yyyyyyyyyyyyy'),(9,'PO1628543705066',6,'2021-08-10 00:00:00','2021-08-10 00:00:00','0','0',NULL,NULL,'9.000000','\0',NULL,'admin','2021-08-10 05:15:05','',NULL,'ggggggggggg');

/*Table structure for table `wms_purchase_order_item` */

DROP TABLE IF EXISTS `wms_purchase_order_item`;

CREATE TABLE `wms_purchase_order_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `purchase_order_id` int(11) DEFAULT NULL COMMENT '订单主表id',
  `product_id` bigint(20) DEFAULT NULL COMMENT '商品id',
  `sku_id` bigint(20) DEFAULT NULL COMMENT 'skuId',
  `warehouse_id` int(11) DEFAULT NULL COMMENT '仓库id',
  `price` decimal(20,6) DEFAULT NULL COMMENT '购买单价',
  `qty` decimal(20,6) DEFAULT NULL COMMENT '购买数量',
  `discount_rate` decimal(20,6) DEFAULT NULL COMMENT '优惠率',
  `discount_amount` decimal(20,6) DEFAULT NULL COMMENT '优惠金额',
  `amount` decimal(20,6) DEFAULT NULL COMMENT '金额',
  `memo` varchar(500) DEFAULT NULL COMMENT '备注',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='采购订单明细表';

/*Data for the table `wms_purchase_order_item` */

insert  into `wms_purchase_order_item`(`id`,`purchase_order_id`,`product_id`,`sku_id`,`warehouse_id`,`price`,`qty`,`discount_rate`,`discount_amount`,`amount`,`memo`,`del_flag`,`tenant_id`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (1,1,NULL,NULL,NULL,'33.000000','2.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-08-05 09:32:45','',NULL,NULL),(2,1,NULL,NULL,NULL,'323.000000','23.000000',NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-08-05 09:32:45','',NULL,NULL),(3,2,NULL,193,2,'859.000000','10.000000',NULL,NULL,NULL,NULL,'\0',6,'13326778347','2021-08-05 14:31:12','',NULL,NULL),(4,2,NULL,194,2,'1859.000000','10.000000',NULL,NULL,NULL,NULL,'\0',6,'13326778347','2021-08-05 14:31:12','',NULL,NULL),(5,3,34,193,2,'859.000000','11.000000','6.000000','66.000000','9449.000000',NULL,'\0',6,'13326778347','2021-08-05 14:37:08','admin','2021-08-09 05:50:37',NULL),(6,3,34,194,2,'1859.000000','32.000000','6.000000','66.000000','59488.000000',NULL,'\0',6,'13326778347','2021-08-05 14:37:08','admin','2021-08-09 05:50:37',NULL),(7,3,34,201,2,'2859.000000','55.000000','6.000000','66.000000','157245.000000',NULL,'\0',6,'admin','2021-08-05 16:29:28','admin','2021-08-09 05:50:37',NULL),(8,4,34,194,2,'677.000000','11.000000',NULL,NULL,'7447.000000',NULL,'\0',NULL,'admin','2021-08-09 05:54:39','admin','2021-08-09 06:02:36',NULL),(9,5,34,193,2,'599.000000','10.000000',NULL,NULL,'5990.000000',NULL,'\0',NULL,'admin','2021-08-09 09:45:51','',NULL,NULL),(10,5,34,193,2,'599.000000','20.000000',NULL,NULL,'11980.000000',NULL,'\0',NULL,'admin','2021-08-09 09:45:51','',NULL,NULL),(11,6,34,194,2,'1599.000000','10.000000',NULL,NULL,'15990.000000',NULL,'\0',NULL,'admin','2021-08-09 10:10:27','',NULL,NULL),(12,6,34,194,2,'1599.000000','20.000000',NULL,NULL,'31980.000000',NULL,'\0',NULL,'admin','2021-08-09 10:10:27','',NULL,NULL),(13,6,34,195,2,'2599.000000','30.000000',NULL,NULL,'77970.000000',NULL,'\0',NULL,'admin','2021-08-09 10:10:27','',NULL,NULL),(14,7,34,194,2,'1599.000000','10.000000',NULL,NULL,'15990.000000',NULL,'\0',NULL,'admin','2021-08-09 10:24:29','',NULL,NULL),(15,8,34,194,2,'333.000000','23.000000',NULL,NULL,'7659.000000',NULL,'\0',NULL,'admin','2021-08-10 05:09:24','',NULL,NULL),(16,9,32,189,2,'3.000000','3.000000',NULL,NULL,'9.000000',NULL,'\0',NULL,'admin','2021-08-10 05:15:05','',NULL,NULL);

/*Table structure for table `wms_sale_order` */

DROP TABLE IF EXISTS `wms_sale_order`;

CREATE TABLE `wms_sale_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) NOT NULL COMMENT '销售单编号',
  `customer_id` int(11) DEFAULT NULL COMMENT '客户id',
  `worker_id` int(11) DEFAULT NULL COMMENT '销售人id',
  `bill_date` datetime DEFAULT NULL COMMENT '单据日期',
  `address` varchar(500) DEFAULT NULL COMMENT '客户地址',
  `inventory_status` varchar(8) DEFAULT NULL COMMENT '0-未确认,1-确认入库,2-确认出库',
  `inventory_type` varchar(8) DEFAULT NULL COMMENT '1-销售出库',
  `discount_rate` decimal(20,6) DEFAULT NULL COMMENT '优惠率',
  `discount_amount` decimal(20,6) DEFAULT NULL COMMENT '优惠金额',
  `total_amount` decimal(20,6) DEFAULT NULL COMMENT '合计金额',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='销售订单表';

/*Data for the table `wms_sale_order` */

insert  into `wms_sale_order`(`id`,`sn`,`customer_id`,`worker_id`,`bill_date`,`address`,`inventory_status`,`inventory_type`,`discount_rate`,`discount_amount`,`total_amount`,`del_flag`,`tenant_id`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (6,'SO1628289401992',2,NULL,'2021-08-07 00:00:00','rrttrfg','0','1','4.000000','334.000000','3434.000000','',NULL,'admin','2021-08-07 06:36:42','admin','2021-08-07 21:36:12',NULL),(18,'SO1628380716898',2,NULL,'2021-08-08 00:00:00','sdsdd','0','1','3.000000','33.000000','2697.000000','',NULL,'admin','2021-08-08 07:58:37','admin','2021-08-09 10:33:57',NULL),(19,'SO1628381283823',2,NULL,'2021-08-08 00:00:00','dfef','0','1','4.000000','333.000000','20.000000','',NULL,'admin','2021-08-08 08:08:04','admin','2021-08-09 10:33:59','4'),(20,'SO1628381360063',2,NULL,'2021-08-08 00:00:00','wewe','0','1','3.000000','33.000000','16.000000','',NULL,'admin','2021-08-08 08:09:20','admin','2021-08-09 10:34:02',NULL),(21,'SO1628476482152',2,NULL,'2021-08-09 00:00:00','fggrg','2','1',NULL,NULL,'19985.000000','\0',NULL,'admin','2021-08-09 10:34:42','',NULL,NULL);

/*Table structure for table `wms_sale_order_item` */

DROP TABLE IF EXISTS `wms_sale_order_item`;

CREATE TABLE `wms_sale_order_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sale_order_id` int(11) DEFAULT NULL COMMENT '销售订单主表id',
  `product_id` int(11) DEFAULT NULL COMMENT '商品id',
  `sku_id` int(11) DEFAULT NULL COMMENT '商品sku_id',
  `warehouse_id` int(11) DEFAULT NULL COMMENT '仓库id',
  `price` decimal(20,6) DEFAULT NULL COMMENT '销售单价',
  `sale_qty` decimal(20,6) DEFAULT NULL COMMENT '销售数量',
  `discount_rate` decimal(20,6) DEFAULT NULL COMMENT '优惠率',
  `discount_amount` decimal(20,6) DEFAULT NULL COMMENT '优惠金额',
  `amount` decimal(20,6) DEFAULT NULL COMMENT '金额',
  `memo` varchar(500) DEFAULT NULL COMMENT '备注',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='销售订单明细表';

/*Data for the table `wms_sale_order_item` */

insert  into `wms_sale_order_item`(`id`,`sale_order_id`,`product_id`,`sku_id`,`warehouse_id`,`price`,`sale_qty`,`discount_rate`,`discount_amount`,`amount`,`memo`,`del_flag`,`tenant_id`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (9,6,34,193,2,'54.000000',NULL,'4.000000','334.000000',NULL,NULL,'\0',NULL,'admin','2021-08-07 06:36:42','admin','2021-08-07 11:41:10',NULL),(10,6,34,194,2,'45545.000000',NULL,'4.000000','334.000000',NULL,NULL,'\0',NULL,'admin','2021-08-07 06:36:42','admin','2021-08-07 11:41:10',NULL),(11,6,34,201,2,'6.000000',NULL,NULL,NULL,NULL,NULL,'\0',NULL,'admin','2021-08-07 11:41:10','',NULL,NULL),(12,18,34,193,2,'899.000000','3.000000','3.000000','33.000000','2697.000000',NULL,'\0',NULL,'admin','2021-08-08 07:58:37','admin','2021-08-09 05:59:35',NULL),(13,19,34,193,2,'4.000000','5.000000','4.000000','333.000000','20.000000',NULL,'\0',NULL,'admin','2021-08-08 08:08:04','admin','2021-08-09 05:59:53',NULL),(14,20,34,201,2,'4.000000','4.000000','3.000000','33.000000','16.000000',NULL,'\0',NULL,'admin','2021-08-08 08:09:20','admin','2021-08-09 06:00:40',NULL),(15,21,34,193,2,'999.000000','10.000000',NULL,NULL,'9990.000000',NULL,'\0',NULL,'admin','2021-08-09 10:34:42','',NULL,NULL),(16,21,34,194,2,'1999.000000','5.000000',NULL,NULL,'9995.000000',NULL,'\0',NULL,'admin','2021-08-09 10:34:42','',NULL,NULL);

/*Table structure for table `wms_supplier` */

DROP TABLE IF EXISTS `wms_supplier`;

CREATE TABLE `wms_supplier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) NOT NULL COMMENT '供应商编号',
  `supplier_name` varchar(30) NOT NULL COMMENT '客户名称',
  `supplier_type` varchar(8) DEFAULT NULL COMMENT '客户类别',
  `balance_date` datetime DEFAULT NULL COMMENT '余额日期',
  `first_pay` decimal(20,6) DEFAULT NULL COMMENT '期初应付',
  `first_pre_pay` decimal(20,6) DEFAULT NULL COMMENT '期初预付款',
  `tax_identity` varchar(64) DEFAULT NULL COMMENT '纳税人识别号',
  `tax_rate` decimal(20,6) DEFAULT NULL COMMENT '增值税税率',
  `bank_info` varchar(256) DEFAULT NULL COMMENT '开户银行',
  `bank_num` varchar(256) DEFAULT NULL COMMENT '银行账号',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='供应商信息表';

/*Data for the table `wms_supplier` */

insert  into `wms_supplier`(`id`,`sn`,`supplier_name`,`supplier_type`,`balance_date`,`first_pay`,`first_pre_pay`,`tax_identity`,`tax_rate`,`bank_info`,`bank_num`,`del_flag`,`tenant_id`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (1,'ST1620692435561','sdssd','1',NULL,NULL,NULL,'',NULL,'','','\0',NULL,'admin','2021-05-11 08:20:36','',NULL,NULL),(3,'ST1620692715001','wefwef','1','2021-05-10 00:00:00','32.000000','23.000000','er','32.000000','eww','ewewe','\0',NULL,'admin','2021-05-11 08:25:15','',NULL,NULL),(4,'ST1620874616655','fghfhg','2','2021-05-12 00:00:00','56.000000','56.000000','56ytyy','56.000000','hght','tytydddccccddddd','\0',NULL,'admin','2021-05-13 10:56:57','admin','2021-05-13 11:10:50',NULL),(5,'ST1620875642455','','',NULL,NULL,NULL,'',NULL,'','','',NULL,'admin','2021-05-13 11:14:02','',NULL,NULL),(6,'ST1628130187008','小米科技','3','2021-08-26 00:00:00','100000.000000','2000.000000','23234444223','2.000000','中国银行','Te323233333','\0',6,'13326778347','2021-08-05 10:23:07','',NULL,NULL);

/*Table structure for table `wms_supplier_contacts` */

DROP TABLE IF EXISTS `wms_supplier_contacts`;

CREATE TABLE `wms_supplier_contacts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(64) NOT NULL COMMENT '联系人姓名',
  `phone` varchar(30) NOT NULL COMMENT '联系人手机',
  `tel` varchar(64) DEFAULT NULL COMMENT '坐机',
  `email` varchar(256) DEFAULT NULL COMMENT '邮箱/QQ/微信',
  `address` varchar(256) DEFAULT NULL COMMENT '地址',
  `is_default` bit(1) DEFAULT NULL COMMENT '是否默认',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `supplier_id` int(11) DEFAULT NULL COMMENT '客户ID',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='供应商联系人信息表';

/*Data for the table `wms_supplier_contacts` */

insert  into `wms_supplier_contacts`(`id`,`name`,`phone`,`tel`,`email`,`address`,`is_default`,`del_flag`,`supplier_id`,`tenant_id`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (1,'fewf','wefw','wefw','wef','wfw','\0','\0',3,NULL,'admin','2021-05-11 08:25:15','',NULL,NULL),(2,'hgfh','ghfgh','fgh','fgh','fgh','\0','\0',4,NULL,'admin','2021-05-13 10:56:57','admin','2021-05-13 11:10:50',NULL),(3,'雷军','15112121422','','','',NULL,'\0',6,6,'13326778347','2021-08-05 10:23:07','',NULL,'');

/*Table structure for table `wms_transfer_order` */

DROP TABLE IF EXISTS `wms_transfer_order`;

CREATE TABLE `wms_transfer_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sn` varchar(64) NOT NULL COMMENT '调拨单编号',
  `bill_date` datetime DEFAULT NULL COMMENT '单据日期',
  `memo` varchar(500) DEFAULT NULL COMMENT '备注',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='调拨单';

/*Data for the table `wms_transfer_order` */

/*Table structure for table `wms_transfer_order_item` */

DROP TABLE IF EXISTS `wms_transfer_order_item`;

CREATE TABLE `wms_transfer_order_item` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `transfer_order_id` int(11) DEFAULT NULL COMMENT '调拨单id',
  `warehouse_in_id` int(11) DEFAULT NULL COMMENT '入库仓',
  `warehouse_out_id` int(11) DEFAULT NULL COMMENT '出库仓',
  `sku_id` int(11) DEFAULT NULL COMMENT '商品sku',
  `qty` decimal(20,6) DEFAULT NULL COMMENT '调拨数量',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='调拨单明细';

/*Data for the table `wms_transfer_order_item` */

/*Table structure for table `wms_warehouse` */

DROP TABLE IF EXISTS `wms_warehouse`;

CREATE TABLE `wms_warehouse` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) NOT NULL COMMENT '仓库编号',
  `warehouse_name` varchar(30) NOT NULL COMMENT '仓库名称',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='仓库信息表';

/*Data for the table `wms_warehouse` */

insert  into `wms_warehouse`(`id`,`sn`,`warehouse_name`,`del_flag`,`tenant_id`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (1,'WT1622637445069','fggg','\0',NULL,'','2021-06-02 20:37:25','',NULL,NULL),(2,'WT1628131586357','广州仓','\0',6,'','2021-08-05 10:46:26','',NULL,NULL);

/*Table structure for table `wms_worker` */

DROP TABLE IF EXISTS `wms_worker`;

CREATE TABLE `wms_worker` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) NOT NULL COMMENT '职员编号',
  `worker_name` varchar(30) NOT NULL COMMENT '职员名称',
  `phone` varchar(64) NOT NULL COMMENT '手机号',
  `del_flag` bit(1) DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='职员信息表';

/*Data for the table `wms_worker` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
