package com.wms.shop.api.factory;

import com.wms.common.core.domain.R;
import com.wms.shop.api.RemoteWxUserService;
import com.wms.shop.api.model.WxLoginDTO;
import com.wms.shop.api.model.WxLoginUser;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 微信用户登录降级处理
 */
@Component
@Slf4j
public class RemoteWxUserFallbackFactory implements FallbackFactory<RemoteWxUserService> {
    @Override
    public RemoteWxUserService create(Throwable throwable) {
        log.error("微信用户登录失败:{}", throwable.getMessage());

        return new RemoteWxUserService() {
            @Override
            public R<WxLoginUser> getWxUserInfo(WxLoginDTO wxLoginDTO) {
                return R.fail("微信用户登录失败:" + throwable.getMessage());
            }
        };
    }
}
