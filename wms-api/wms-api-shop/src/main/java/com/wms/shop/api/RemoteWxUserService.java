package com.wms.shop.api;

import com.wms.common.core.constant.ServiceNameConstants;
import com.wms.common.core.domain.R;
import com.wms.shop.api.factory.RemoteWxUserFallbackFactory;
import com.wms.shop.api.model.WxLoginDTO;
import com.wms.shop.api.model.WxLoginUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(contextId = "remoteWxUserService", value = ServiceNameConstants.SHOP_SERVICE, fallbackFactory = RemoteWxUserFallbackFactory.class)
public interface RemoteWxUserService {

    /**
     * 通过用户名查询用户信息
     *
     * @param wxLoginDTO loginMaDTO
     * @return 结果
     */
    @PostMapping(value = "/wx/user/info")
    R<WxLoginUser> getWxUserInfo(@RequestBody WxLoginDTO wxLoginDTO);
}
