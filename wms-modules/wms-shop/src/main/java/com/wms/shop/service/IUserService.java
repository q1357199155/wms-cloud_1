package com.wms.shop.service;

import java.util.List;
import com.wms.shop.domain.User;
import com.wms.shop.api.domain.WxUser;

/**
 * 商城用户Service接口
 * 
 * @author zzm
 * @date 2021-08-18
 */
public interface IUserService 
{
    /**
     * 查询商城用户
     * 
     * @param id 商城用户ID
     * @return 商城用户
     */
     User selectUserById(Long id);

    /**
     * 根据openid 查询用户
     * @param openid
     * @return
     */
     WxUser selectUserByOpenid(String openid);

    /**
     * 查询商城用户列表
     * 
     * @param user 商城用户
     * @return 商城用户集合
     */
     List<User> selectUserList(User user);

    /**
     * 新增商城用户
     * 
     * @param user 商城用户
     * @return 结果
     */
     int insertUser(User user);

    /**
     * 修改商城用户
     * 
     * @param user 商城用户
     * @return 结果
     */
     int updateUser(User user);

    /**
     * 批量删除商城用户
     * 
     * @param ids 需要删除的商城用户ID
     * @return 结果
     */
     int deleteUserByIds(Long[] ids);

    /**
     * 删除商城用户信息
     * 
     * @param id 商城用户ID
     * @return 结果
     */
     int deleteUserById(Long id);


    /**
     * 微信登录
     * @param appId
     * @param code
     * @return
     */
     WxUser wxLogin(String appId, String code);
}
