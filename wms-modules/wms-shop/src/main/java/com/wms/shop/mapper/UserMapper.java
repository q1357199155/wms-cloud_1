package com.wms.shop.mapper;

import com.wms.shop.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 商城用户Mapper接口
 * 
 * @author zzm
 * @date 2021-08-18
 */
public interface UserMapper extends BaseMapper<User>
{

}
