package com.wms.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wms.shop.api.domain.WxUser;

/**
 * 商城用户Mapper接口
 * 
 * @author zzm
 * @date 2021-08-18
 */
public interface WxUserMapper extends BaseMapper<WxUser>
{

}
