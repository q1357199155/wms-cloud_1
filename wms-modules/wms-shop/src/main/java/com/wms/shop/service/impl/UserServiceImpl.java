package com.wms.shop.service.impl;

import java.util.List;
import java.util.Arrays;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import com.wms.common.core.exception.CustomException;
import com.wms.common.core.utils.DateUtils;
import com.wms.shop.api.domain.WxUser;
import com.wms.shop.config.WxMaConfiguration;
import com.wms.shop.mapper.WxUserMapper;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wms.common.core.utils.SecurityUtils;
import com.wms.shop.mapper.UserMapper;
import com.wms.shop.domain.User;
import com.wms.shop.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.transaction.annotation.Transactional;

/**
 * 商城用户Service业务层处理
 * 
 * @author zzm
 * @date 2021-08-18
 */
@Service
public class UserServiceImpl implements IUserService 
{
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private WxUserMapper wxUserMapper;

    /**
     * 查询商城用户
     * 
     * @param id 商城用户ID
     * @return 商城用户
     */
    @Override
    public User selectUserById(Long id)
    {
        return userMapper.selectById(id);
    }

    /**
     * 根据openid 查询用户
     * @param openid
     * @return
     */
    @Override
    public WxUser selectUserByOpenid(String openid) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("openid", openid);
        return wxUserMapper.selectOne(queryWrapper);
    }

    /**
     * 查询商城用户列表
     * 
     * @param user 商城用户
     * @return 商城用户
     */
    @Override
    public List<User> selectUserList(User user)
    {
        QueryWrapper queryWrapper = new QueryWrapper();
        return userMapper.selectList(queryWrapper);
    }

    /**
     * 新增商城用户
     * 
     * @param user 商城用户
     * @return 结果
     */
    @Override
    public int insertUser(User user)
    {
        user.setDelFlag(false);
        user.setCreateBy(user.getUserName());
        user.setCreateTime(DateUtils.getNowDate());
        return userMapper.insert(user);
    }

    /**
     * 修改商城用户
     * 
     * @param user 商城用户
     * @return 结果
     */
    @Override
    public int updateUser(User user)
    {
        user.setUpdateBy(user.getUserName());
        user.setUpdateTime(DateUtils.getNowDate());
        return userMapper.updateById(user);
    }

    /**
     * 批量删除商城用户
     * 
     * @param ids 需要删除的商城用户ID
     * @return 结果
     */
    @Override
    public int deleteUserByIds(Long[] ids)
    {
        User user = new User();
        user.setDelFlag(Boolean.TRUE);
        user.setUpdateTime(DateUtils.getNowDate());
        user.setUpdateBy(SecurityUtils.getUsername());
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("id",Arrays.asList(ids));
        return userMapper.update(user, queryWrapper);
    }

    /**
     * 删除商城用户信息
     * 
     * @param id 商城用户ID
     * @return 结果
     */
    @Override
    public int deleteUserById(Long id)
    {
        return userMapper.deleteById(id);
    }

    /**
     * 微信登录
     * @param appId
     * @param code
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public WxUser wxLogin(String appId, String code) {
        final WxMaService wxService = WxMaConfiguration.getMaService(appId);
        String openid = "";
        try {
            WxMaJscode2SessionResult session = wxService.getUserService().getSessionInfo(code);
            openid = session.getOpenid();
        } catch (WxErrorException e) {
            throw new CustomException("获取微信openid失败");
        }
        WxUser wxUser = this.selectUserByOpenid(openid);
        if (wxUser == null) {
            User user = new User();
            user.setOpenid(openid);
            this.insertUser(user);
            wxUser = this.selectUserByOpenid(openid);
        }
        return wxUser;
    }
}
