package com.wms.oms.controller.weixin;


import com.wms.common.core.web.controller.BaseController;
import com.wms.common.core.web.domain.AjaxResult;
import com.wms.oms.domain.dto.ProductDto;
import com.wms.oms.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 微信小程序调用商品接口
 */

@RestController
@RequestMapping("/wx/product")
public class ProductWxController extends BaseController {


    @Autowired
    private IProductService productService;

    /**
     * 查询商品DTO信息列表
     */
    @GetMapping("/list")
    public AjaxResult list(ProductDto productDto){
        List<ProductDto> list = productService.selectProductDtoList(productDto);
        return AjaxResult.success(list);
    }
}
