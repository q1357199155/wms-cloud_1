package com.wms.oms.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import com.wms.common.core.web.domain.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

/**
 * 销售订单明细对象 wms_sale_order_item
 *
 * @author zzm
 * @date 2021-05-16
 */
@Data
@TableName("wms_sale_order_item")
public class SaleOrderItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 销售订单主表id */
    private Long saleOrderId;

    /** 商品id */
    private Long productId;

    /** sku_id */
    private Long skuId;

    /** 仓库id */
    private Long warehouseId;

    /** 销售单价 */
    @NotNull(message = "销售单价不能为空")
    @Digits(integer = 20,fraction = 4,message = "请输入有效数字")
    @DecimalMin(value = "0",message = "销售单价必须是数字且必须不小于0")
    private BigDecimal price;

    /** 销售数量 */
    @NotNull(message = "销售数量不能为空")
    @Digits(integer = 20,fraction = 4,message = "请输入有效数字")
    @DecimalMin(value = "0",message = "销售数量必须是数字且必须不小于0")
    private BigDecimal saleQty;

    /** 优惠率 */
    private BigDecimal discountRate;

    /** 优惠金额 */
    private BigDecimal discountAmount;

    /** 金额 */
    private BigDecimal amount;

    /** 备注 */
    private String memo;

    /** 删除标志（0代表存在 1代表删除） */
    private Boolean delFlag;

    /** 租户ID */
    private Long tenantId;

    /** 关联商品对象 */
    @TableField(exist = false)
    private Product product;

    /** 关联商品sku对象 */
    @TableField(exist = false)
    private ProductSku productSku;

    /** 关联仓库 */
    @TableField(exist = false)
    private Warehouse warehouse;

    /** 关联库存 */
    @TableField(exist = false)
    private Inventory inventory;

}