package com.wms.oms.service.impl;

import java.util.List;
import java.util.Arrays;

import com.wms.common.core.exception.CustomException;
import com.wms.common.core.utils.DateUtils;
import com.wms.oms.domain.*;
import com.wms.oms.enums.InventoryStatus;
import com.wms.oms.enums.InventoryType;
import com.wms.oms.mapper.InventoryLogMapper;
import com.wms.oms.mapper.SaleOrderItemMapper;
import com.wms.oms.service.IInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wms.common.core.utils.SecurityUtils;
import com.wms.oms.mapper.SaleOrderMapper;
import com.wms.oms.service.ISaleOrderService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.transaction.annotation.Transactional;

/**
 * 销售订单Service业务层处理
 *
 * @author zzm
 * @date 2021-05-16
 */
@Service
public class SaleOrderServiceImpl implements ISaleOrderService
{
    @Autowired
    private SaleOrderMapper saleOrderMapper;

    @Autowired
    private SaleOrderItemMapper saleOrderItemMapper;

    @Autowired
    private InventoryLogMapper inventoryLogMapper;

    @Autowired
    private IInventoryService inventoryService;


    /**
     * 查询销售订单
     *
     * @param id 销售订单ID
     * @return 销售订单
     */
    @Override
    public SaleOrder selectSaleOrderById(Long id)
    {
        return saleOrderMapper.selectById(id);
    }

    /**
     * 查询销售订单列表
     *
     * @param saleOrder 销售订单
     * @return 销售订单
     */
    @Override
    public List<SaleOrder> selectSaleOrderList(SaleOrder saleOrder)
    {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("wso.del_flag", false);
        queryWrapper.orderByDesc("wso.id");
        return saleOrderMapper.selectList(queryWrapper);
    }

    /**
     * 新增销售订单
     *
     * @param saleOrder 销售订单
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSaleOrder(SaleOrder saleOrder)
    {
        saleOrder.setSn("SO"+DateUtils.getNowDate().getTime());
        saleOrder.setDelFlag(false);
        saleOrder.setCreateBy(SecurityUtils.getUsername());
        saleOrder.setCreateTime(DateUtils.getNowDate());
        saleOrder.setInventoryType(InventoryType.SALE_OUT.getValue());
        saleOrder.setInventoryStatus(InventoryStatus.NOT_CONFIRM.getValue());
        int result = saleOrderMapper.insert(saleOrder);
        for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItemList()) {
            if (saleOrderItem.getInventory().getQty().compareTo(saleOrderItem.getSaleQty()) == -1) {
                 throw new CustomException("库存不足！");
            }
            saleOrderItem.setSaleOrderId(saleOrder.getId());
            saleOrderItem.setCreateBy(saleOrder.getCreateBy());
            saleOrderItem.setCreateTime(saleOrder.getCreateTime());
            saleOrderItem.setDelFlag(Boolean.FALSE);
            saleOrderItemMapper.insert(saleOrderItem);
        }
        return result;
    }

    /**
     * 修改销售订单
     *
     * @param saleOrder 销售订单
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateSaleOrder(SaleOrder saleOrder) {
        saleOrder.setUpdateBy(SecurityUtils.getUsername());
        saleOrder.setUpdateTime(DateUtils.getNowDate());
        for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItemList()) {
            if (saleOrderItem.getId() == null) {
                saleOrderItem.setSaleOrderId(saleOrder.getId());
                saleOrderItem.setCreateBy(saleOrder.getUpdateBy());
                saleOrderItem.setCreateTime(saleOrder.getUpdateTime());
                saleOrderItem.setDelFlag(Boolean.FALSE);
                saleOrderItemMapper.insert(saleOrderItem);
            } else {
                saleOrderItem.setUpdateBy(saleOrder.getUpdateBy());
                saleOrderItem.setUpdateTime(saleOrder.getUpdateTime());
                saleOrderItemMapper.updateById(saleOrderItem);
            }
        }
        return saleOrderMapper.updateById(saleOrder);
    }


    /**
     * 确认出库
     *
     * @param saleOrder 销售订单
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int confirm(SaleOrder saleOrder)
    {
        for(SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItemList()){
            Inventory inventory  = inventoryService.selectInventoryByPro(saleOrderItem.getSkuId(), saleOrderItem.getWarehouseId());
            if(inventory == null){
                throw new CustomException("skuId:"+saleOrderItem.getSkuId()+"--无库存！");
            }else{
                if (inventory.getQty().compareTo(saleOrderItem.getSaleQty()) == -1) {
                    throw new CustomException("skuId:"+saleOrderItem.getSkuId()+"--超库存！");
                }
                inventory.setQty(inventory.getQty().subtract(saleOrderItem.getSaleQty()));
                inventoryService.updateInventory(inventory);
            }
            // 库存日志
            InventoryLog inventoryLog = new InventoryLog();
            inventoryLog.setInventoryType(saleOrder.getInventoryType());
            inventoryLog.setSkuId(saleOrderItem.getSkuId());
            inventoryLog.setDelFlag(Boolean.FALSE);
            inventoryLog.setWarehouseId(saleOrderItem.getWarehouseId());
            inventoryLog.setPrice(saleOrderItem.getPrice());
            inventoryLog.setQty(saleOrderItem.getSaleQty());
            inventoryLog.setSn(saleOrder.getSn());
            inventoryLog.setCreateTime(inventory.getCreateTime());
            inventoryLog.setCreateBy(SecurityUtils.getUsername());
            inventoryLogMapper.insert(inventoryLog);
        }
        // 确认入库
        saleOrder.setInventoryStatus(InventoryStatus.CONFIRM_OUT.getValue());
        return saleOrderMapper.updateById(saleOrder);
    }

    /**
     * 批量删除销售订单
     *
     * @param ids 需要删除的销售订单ID
     * @return 结果
     */
    @Override
    public int deleteSaleOrderByIds(Long[] ids)
    {
        SaleOrder saleOrder = new SaleOrder();
        saleOrder.setDelFlag(Boolean.TRUE);
        saleOrder.setUpdateTime(DateUtils.getNowDate());
        saleOrder.setUpdateBy(SecurityUtils.getUsername());
        QueryWrapper<SaleOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("id",Arrays.asList(ids));
        return saleOrderMapper.update(saleOrder, queryWrapper);
    }

    /**
     * 删除销售订单信息
     *
     * @param id 销售订单ID
     * @return 结果
     */
    @Override
    public int deleteSaleOrderById(Long id)
    {
        return saleOrderMapper.deleteById(id);
    }
}