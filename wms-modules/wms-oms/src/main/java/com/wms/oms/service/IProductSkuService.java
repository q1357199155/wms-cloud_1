package com.wms.oms.service;

import java.util.List;
import com.wms.oms.domain.ProductSku;

/**
 * 商品sku信息Service接口
 *
 * @author zzm
 * @date 2021-05-15
 */
public interface IProductSkuService {


    /**
     * 查询商品sku信息列表
     *
     * @param productSku 商品sku信息
     * @return 商品sku信息集合
     */
    List<ProductSku> selectProductSkuList(ProductSku productSku);

}